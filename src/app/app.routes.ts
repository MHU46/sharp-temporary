import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NgModule} from '@angular/core';

/*pages */
import {SignupComponent} from './pages/signup/signup.component';
import {LoginComponent} from './pages/login/login.component';
import {ResetComponent} from './pages/reset/reset.component';
import {ForgotComponent} from './pages/forgot/forgot.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HotItemsComponent } from './pages/hot-items/hot-items.component';
import { EditHotItemsComponent } from './pages/edit-hot-items/edit-hot-items.component';
import { AllAccountsComponent } from './pages/all-accounts/all-accounts.component';
import { InventoryComponent } from './pages/inventory/inventory.component';
import { TradesComponent } from './pages/trades/trades.component';
import { TransfersComponent } from './pages/transfers/transfers.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { UploadOrdersComponent } from './pages/upload-orders/upload-orders.component';
import { PaymentsComponent } from './pages/payments/payments.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { ItemPricingComponent } from './pages/pricing/item-pricing/item-pricing.component';
import { ClientPricingComponent } from './pages/pricing/client-pricing/client-pricing.component';
import { PriceExceptionComponent } from './pages/pricing/price-exception/price-exception.component';
import { SPriceImportComponent } from './pages/pricing/s-price-import/s-price-import.component';
import { OrderPicsComponent } from './pages/s3 Logs/order-pics/order-pics.component';
import { UltimatePicsComponent } from './pages/s3 Logs/ultimate-pics/ultimate-pics.component';
import { UserLedgerComponent } from './pages/user-ledger/user-ledger.component';
import { MedicinesComponent } from './pages/medicines/medicines.component';
import { ProductGalleryComponent } from './pages/product-gallery/product-gallery.component';
import { ClientInventoryComponent } from './pages/client-inventory/client-inventory.component';
import { ClientTradesComponent } from './pages/client-trades/client-trades.component';
import { ClientTransferComponent } from './pages/client-transfer/client-transfer.component';
import { NewOrderComponent } from './pages/client-orders/new-order/new-order.component';
import { EditOrderComponent } from './pages/client-orders/edit-order/edit-order.component';
import { TransitOrderComponent } from './pages/client-orders/transit-order/transit-order.component';
import { HoldOrderComponent } from './pages/client-orders/hold-order/hold-order.component';
import { AllOrdersComponent } from './pages/client-orders/all-orders/all-orders.component';
import { NewClientInvoiceComponent } from './pages/client-invoices/new-client-invoice/new-client-invoice.component';
import { AllClientInvoiceComponent } from './pages/client-invoices/all-client-invoice/all-client-invoice.component';
import { ClientBillingComponent } from './pages/client-payments/client-billing/client-billing.component';
import { ClientLedgerComponent } from './pages/client-payments/client-ledger/client-ledger.component';
import { ClientAddPaymentComponent } from './pages/client-payments/client-add-payment/client-add-payment.component';
import { ClientAllPaymentsComponent } from './pages/client-payments/client-all-payments/client-all-payments.component';
import { StaffInventoryComponent } from './pages/staff/staff-inventory/staff-inventory.component';
import { StaffOrdersComponent } from './pages/staff/staff-orders/staff-orders.component';
import { StaffTrackComponent } from './pages/staff/staff-track/staff-track.component';
import { StaffUploadPicComponent } from './pages/staff/staff-upload-pic/staff-upload-pic.component';
import { StaffVerifyOrderComponent } from './pages/staff/staff-verify-order/staff-verify-order.component';
import { StaffMedicinesComponent } from './pages/staff/staff-medicines/staff-medicines.component';
import { StaffTradesComponent } from './pages/staff/staff-trades/staff-trades.component';
import { StaffTransitsComponent } from './pages/staff/staff-transits/staff-transits.component';
import { StaffTransfersComponent } from './pages/staff/staff-transfers/staff-transfers.component';
import { StaffBalancesComponent } from './pages/staff/staff-balances/staff-balances.component';
import { StaffUnusedTrackComponent } from './pages/staff/staff-unused-track/staff-unused-track.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';

export const mianRouting: Routes = [
  {path: '', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'login', component: LoginComponent},
  {path:  'reset', component: ResetComponent},
  {path:  'forgot', component: ForgotComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'hotItems', component: HotItemsComponent},
  {path: 'editHotItems', component: EditHotItemsComponent},
  {path: 'accounts', component: AllAccountsComponent},
  {path: 'inventory', component: InventoryComponent},
  {path: 'trades', component: TradesComponent},
  {path: 'transfer', component: TransfersComponent},
  {path: 'orders', component: OrdersComponent},
  {path: 'uploadOrders', component: UploadOrdersComponent}, 
  {path: 'payments', component: PaymentsComponent},
  {path: 'invoices', component: InvoicesComponent},
  {path: 'itemPricing', component: ItemPricingComponent},
  {path: 'clientPricing', component: ClientPricingComponent},
  {path: 'priceExceptions', component: PriceExceptionComponent},
  {path: 'PriceImport', component: SPriceImportComponent},
  {path: 'orderPics', component: OrderPicsComponent},
  {path: 'ultimatePics', component: UltimatePicsComponent},
  {path: 'userBalances', component: UserLedgerComponent},
  {path: 'medicines', component: MedicinesComponent},
  {path: 'productGallery', component: ProductGalleryComponent},
  {path: 'clientInventory', component: ClientInventoryComponent},
  {path: 'allTrades', component: ClientTradesComponent},
  {path: 'allTransfers', component: ClientTransferComponent},
  {path: 'orders/new', component: NewOrderComponent},
  {path: 'editOrders', component: EditOrderComponent},
  {path: 'transitOrders', component: TransitOrderComponent},
  {path: 'holdOrders', component: HoldOrderComponent},
  {path: 'allOrders', component: AllOrdersComponent},
  {path: 'newInvoice', component: NewClientInvoiceComponent},
  {path: 'allInvoices', component: AllClientInvoiceComponent},
  {path: 'clientBilling', component: ClientBillingComponent},
  {path: 'clientLedger', component: ClientLedgerComponent},
  {path: 'newPayment', component: ClientAddPaymentComponent},
  {path: 'allPayments', component: ClientAllPaymentsComponent},
  {path: 'orderInventory', component: StaffInventoryComponent},
  {path: 'orderProcessing', component: StaffOrdersComponent},
  {path: 'trackProcessing', component: StaffTrackComponent},
  {path: 'uploadOrderPics', component: StaffUploadPicComponent},
  {path: 'verifyOrders', component: StaffVerifyOrderComponent},
  {path: 'staff/productGallery', component: StaffMedicinesComponent},
  {path: 'staff/trades', component: StaffTradesComponent},
  {path: 'saff/transits', component: StaffTransitsComponent},
  {path: 'staff/transfers', component: StaffTransfersComponent},
  {path: 'staff/ledger', component: StaffBalancesComponent},
  {path: 'markTrackingUsed', component: StaffUnusedTrackComponent},
  {path: 'user', component: UserProfileComponent},

  ]
@NgModule({
	imports : [ RouterModule.forRoot(mianRouting)],
	exports : [RouterModule]
})
export class mainRoutingModule{
}
