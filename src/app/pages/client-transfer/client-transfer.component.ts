import { Component, OnInit } from '@angular/core';
import  { uTransfersService } from '../../models/uTransfer.service';
@Component({
  selector: 'app-client-transfer',
  templateUrl: './client-transfer.component.html',
  styleUrls: ['./client-transfer.component.css']
})
export class ClientTransferComponent implements OnInit {

  constructor(  private uTransfersService: uTransfersService ) { }
  item1:  any = 'INR';
  user: any = 'sellergyan';
  role: any = 'Admin';
  ngOnInit() {
    this.getClientTransfers();
  }
  getClientTransfers(){
    this.uTransfersService.clientTransfers(this.item1,this.user,this.role).subscribe(res=>{
    console.log(res)
    })
  }
}
