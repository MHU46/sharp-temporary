import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../models/clients.service';

@Component({
  selector: 'app-user-ledger',
  templateUrl: './user-ledger.component.html',
  styleUrls: ['./user-ledger.component.css']
})
export class UserLedgerComponent implements OnInit {

  constructor(  private clientService: ClientService  ) { }

  ngOnInit() {
    this.getAllAccounts();
  }
  getAllAccounts(){
    this.clientService.getAllClientAccounts().subscribe(res=>{
      console.log(res);
    })
  }
}
 