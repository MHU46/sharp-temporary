import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../models/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  constructor(  private ordersService: OrdersService  ) { }

  ngOnInit() {
    this.getRecentOrders();
  }
  getRecentOrders(){
    this.ordersService.RecentOrders().subscribe(res=>{
      console.log(res)
    })
  }
}
