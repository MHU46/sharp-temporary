import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffTrackComponent } from './staff-track.component';

describe('StaffTrackComponent', () => {
  let component: StaffTrackComponent;
  let fixture: ComponentFixture<StaffTrackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffTrackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
