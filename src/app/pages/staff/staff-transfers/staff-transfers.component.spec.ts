import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffTransfersComponent } from './staff-transfers.component';

describe('StaffTransfersComponent', () => {
  let component: StaffTransfersComponent;
  let fixture: ComponentFixture<StaffTransfersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffTransfersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffTransfersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
