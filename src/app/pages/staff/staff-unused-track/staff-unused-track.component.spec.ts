import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffUnusedTrackComponent } from './staff-unused-track.component';

describe('StaffUnusedTrackComponent', () => {
  let component: StaffUnusedTrackComponent;
  let fixture: ComponentFixture<StaffUnusedTrackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffUnusedTrackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffUnusedTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
