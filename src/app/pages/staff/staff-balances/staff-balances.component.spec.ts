import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffBalancesComponent } from './staff-balances.component';

describe('StaffBalancesComponent', () => {
  let component: StaffBalancesComponent;
  let fixture: ComponentFixture<StaffBalancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffBalancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffBalancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
