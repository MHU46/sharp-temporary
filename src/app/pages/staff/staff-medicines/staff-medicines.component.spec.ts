import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffMedicinesComponent } from './staff-medicines.component';

describe('StaffMedicinesComponent', () => {
  let component: StaffMedicinesComponent;
  let fixture: ComponentFixture<StaffMedicinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffMedicinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffMedicinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
