import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffVerifyOrderComponent } from './staff-verify-order.component';

describe('StaffVerifyOrderComponent', () => {
  let component: StaffVerifyOrderComponent;
  let fixture: ComponentFixture<StaffVerifyOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffVerifyOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffVerifyOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
