import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffTransitsComponent } from './staff-transits.component';

describe('StaffTransitsComponent', () => {
  let component: StaffTransitsComponent;
  let fixture: ComponentFixture<StaffTransitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffTransitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffTransitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
