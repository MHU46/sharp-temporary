import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffTradesComponent } from './staff-trades.component';

describe('StaffTradesComponent', () => {
  let component: StaffTradesComponent;
  let fixture: ComponentFixture<StaffTradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffTradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffTradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
