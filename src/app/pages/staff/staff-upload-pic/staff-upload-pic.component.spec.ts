import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffUploadPicComponent } from './staff-upload-pic.component';

describe('StaffUploadPicComponent', () => {
  let component: StaffUploadPicComponent;
  let fixture: ComponentFixture<StaffUploadPicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffUploadPicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffUploadPicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
