import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllClientInvoiceComponent } from './all-client-invoice.component';

describe('AllClientInvoiceComponent', () => {
  let component: AllClientInvoiceComponent;
  let fixture: ComponentFixture<AllClientInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllClientInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllClientInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
