import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewClientInvoiceComponent } from './new-client-invoice.component';

describe('NewClientInvoiceComponent', () => {
  let component: NewClientInvoiceComponent;
  let fixture: ComponentFixture<NewClientInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewClientInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewClientInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
