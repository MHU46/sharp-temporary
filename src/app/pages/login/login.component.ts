import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AuthService } from '../../models/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: any;
  password: any;

  constructor(private authenticationService : AuthService,
              private router : Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }
  
  login(){
    this.authenticationService.login(this.username,this.password).subscribe(res =>{
      console.log(res)
      if(res.status == 'success'){
        this.router.navigate(['/dashboard']);
      }
      else if(res.status=='failed'){
        alert(res.message)
      }
    }) 
  }

}
