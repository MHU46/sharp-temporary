import { Component, OnInit } from '@angular/core';
import { LogsService } from '../../models/logs.service';
 
@Component({
  selector: 'app-all-accounts',
  templateUrl: './all-accounts.component.html',
  styleUrls: ['./all-accounts.component.css']
})
export class AllAccountsComponent implements OnInit {
  RES:any;
  data:any;
  constructor( private logsService: LogsService ) { }

  ngOnInit() {
    this.getAllAccountBalance();
  }
  
  getAllAccountBalance(){
    this.logsService.getAllAccountBalance().subscribe(res=>{
      this.RES = res;
      this.data = this.RES.data;
    })
  }
}
