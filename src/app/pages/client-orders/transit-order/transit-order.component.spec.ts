import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransitOrderComponent } from './transit-order.component';

describe('TransitOrderComponent', () => {
  let component: TransitOrderComponent;
  let fixture: ComponentFixture<TransitOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransitOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
