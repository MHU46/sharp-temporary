import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientTradesComponent } from './client-trades.component';

describe('ClientTradesComponent', () => {
  let component: ClientTradesComponent;
  let fixture: ComponentFixture<ClientTradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientTradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientTradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
