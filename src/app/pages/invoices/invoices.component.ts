import { Component, OnInit } from '@angular/core';
import { InvoicesService } from '../../models/invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {

  constructor(  private invoicesService: InvoicesService  ) { }

  ngOnInit() {
    this.getRecentInvoices();
  }
  getRecentInvoices(){
    this.invoicesService.RecentInvoices().subscribe(res=>{
      console.log(res)
    })
  }
}
