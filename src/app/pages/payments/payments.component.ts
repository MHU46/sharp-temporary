import { Component, OnInit } from '@angular/core';
import { PaymentsService } from '../../models/payments.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {

  constructor( private paymentsService: PaymentsService ) { }

  ngOnInit() {
    this.getRecentOrders();
  }
  getRecentOrders(){
    this.paymentsService.RecentPayments().subscribe(res=>{
      console.log(res)
    })
  }
}
