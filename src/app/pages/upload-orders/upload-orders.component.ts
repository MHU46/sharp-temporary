import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../models/admin.service';

@Component({
  selector: 'app-upload-orders',
  templateUrl: './upload-orders.component.html',
  styleUrls: ['./upload-orders.component.css']
})
export class UploadOrdersComponent implements OnInit {

  orderId: any = '1Odaq-yV3KNg_JBaySFKgB5LETXR3hubXYkmKhgEUMQY';

  constructor(  private adminService: AdminService ) { }

  ngOnInit() {
    this.uploadOrders();
  }
  uploadOrders(){
    this.adminService.uploadOrderById(this.orderId).subscribe(res=>{
      console.log("upload res"+res);
      if(res){
        alert ("Uploaded Successfully ");
      } else {
        alert("Failed to upload order");
      }
    })
  }
}
