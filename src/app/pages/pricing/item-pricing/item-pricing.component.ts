import { Component, OnInit } from '@angular/core';
import { mItemsService } from '../../../models/mItem.service';

@Component({
  selector: 'app-item-pricing',
  templateUrl: './item-pricing.component.html',
  styleUrls: ['./item-pricing.component.css']
})
export class ItemPricingComponent implements OnInit {

  constructor(  private ItemsService: mItemsService  ) { }

  ngOnInit() {
    this.getItemPricing();
  }
  getItemPricing(){
    this.ItemsService.itemPricing().subscribe(res=>{
      console.log(res)
    })
  }
}
