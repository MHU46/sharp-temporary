import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../../models/clients.service';
import { PriceExceptionService } from '../../../models/priceException.service';

@Component({
  selector: 'app-price-exception',
  templateUrl: './price-exception.component.html',
  styleUrls: ['./price-exception.component.css']
})
export class PriceExceptionComponent implements OnInit {

  constructor(  private clientService: ClientService,
                private priceExceptionService: PriceExceptionService  ) { }

  ngOnInit() {
    this.ActiveClients();
    this.priceException();
  }
  ActiveClients(){
    this.clientService.getActiveClients().subscribe(res=>{
      console.log(res)
    })
  }
  priceException(){
    this.priceExceptionService.getAllPriceException().subscribe(res=>{
      console.log(res)
    })
  }
}
