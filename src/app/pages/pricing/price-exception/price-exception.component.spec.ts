import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceExceptionComponent } from './price-exception.component';

describe('PriceExceptionComponent', () => {
  let component: PriceExceptionComponent;
  let fixture: ComponentFixture<PriceExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
