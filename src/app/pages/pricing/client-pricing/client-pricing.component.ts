import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../../models/clients.service';

@Component({
  selector: 'app-client-pricing',
  templateUrl: './client-pricing.component.html',
  styleUrls: ['./client-pricing.component.css']
})
export class ClientPricingComponent implements OnInit {

  constructor(  private clientService: ClientService  ) { }

  ngOnInit() {
    this.clientPricing();
  }
  clientPricing(){
    this.clientService.getClientPricing().subscribe(res=>{
      console.log(res)
    })
  }
}
