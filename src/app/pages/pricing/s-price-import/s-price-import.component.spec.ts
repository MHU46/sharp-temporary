import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SPriceImportComponent } from './s-price-import.component';

describe('SPriceImportComponent', () => {
  let component: SPriceImportComponent;
  let fixture: ComponentFixture<SPriceImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SPriceImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SPriceImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
