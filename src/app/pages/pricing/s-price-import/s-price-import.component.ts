import { Component, OnInit } from '@angular/core';
import { PriceImportService } from '../../../models/sPriceImport.service';

@Component({
  selector: 'app-s-price-import',
  templateUrl: './s-price-import.component.html',
  styleUrls: ['./s-price-import.component.css']
})
export class SPriceImportComponent implements OnInit {

  constructor(  private priceImportService: PriceImportService  ) { }
  priceRes: any;

  ngOnInit() {
    this.AllPriceImport();
  }
  AllPriceImport(){
    this.priceImportService.getAllPriceImport().subscribe(res=>{
      console.log(res)
      this.priceRes = res.data;
    })
  }
}
