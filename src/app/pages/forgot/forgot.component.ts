import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AuthService } from '../../models/auth.service';
@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  email:any;
  constructor(private authenticationService : AuthService,
              private router : Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }
  forgot(){
    this.authenticationService.forgotPassword(this.email).subscribe(res =>{
      console.log(res)
      if(res.status == 'success'){
        this.router.navigate(['/reset']);
      }
      else if(res.status=='failed'){
        alert(res.message)
      }
    })
  }
}
