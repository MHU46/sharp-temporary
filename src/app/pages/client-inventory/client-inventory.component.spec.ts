import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInventoryComponent } from './client-inventory.component';

describe('ClientInventoryComponent', () => {
  let component: ClientInventoryComponent;
  let fixture: ComponentFixture<ClientInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
