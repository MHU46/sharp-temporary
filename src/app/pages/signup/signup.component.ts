import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AuthService } from '../../models/auth.service';
import { ThrowStmt } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  username: any;
  email: any;
  password: any;
  role:any='Admin';

  constructor(private authenticationService : AuthService,
              private router : Router,
              private route: ActivatedRoute) { }
  ngOnInit() {
  }
  signUp(){
    this.authenticationService.signUp(this.username,this.email,this.password,this.role).subscribe(res =>{
      if(res.status == 'success'){
        alert("Signup Successfully")
        this.router.navigate(['/login']);
      }
      else if(res.status=='failed'){
        alert(res.message)
      }
    }) 
  }
}
