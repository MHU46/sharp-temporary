import { Component, OnInit } from '@angular/core';
import { s3OrderLogsService } from '../../../models/s3OrderLog.service';
import { MaterialService } from '../../../models/material.service';


@Component({
  selector: 'app-order-pics',
  templateUrl: './order-pics.component.html',
  styleUrls: ['./order-pics.component.css']
})
export class OrderPicsComponent implements OnInit {

  startDate: any ;
  endDate: any ;
  dateOffset: any;
  nodata:any
  picsRes:any;
  show: boolean = false;
  image:any;
  orderId: any;
  Client: any;
  selectedRow : Number;
  setClickedRow : Function;

  constructor(  private s3OrderLogsService : s3OrderLogsService,
                private materialService : MaterialService  ) { } 

  ngOnInit() {
    this.getRecentPics();
    this.endDate = this.materialService.getTodayDate();
    this.startDate = this.materialService.getNextDayDate();
  }
 
  public currentCompany;

  public selectRow(event: any, res: any) {
    this.orderId = res.orderId;
  }

  getRecentPics(){
    this.s3OrderLogsService.recentPics().subscribe(res=>{
      if (res.length || res.data.length != 0){
        this.picsRes = res.data;
      }
      else if(res.data.length == 0){
        this.picsRes=res.data.length
      }
      else if(!res){
        alert("Something went wrong please try again")
      }

    })
  }

  getBetweenPics(){
    this.s3OrderLogsService.getBetweenPics( this.startDate,this.endDate).subscribe(res=>{
      this.picsRes = res.data;
      this.getresponse();
    })
    this.setClickedRow = function(index){
    this.selectedRow = index;
  }
  }

  getresponse(){
    this.s3OrderLogsService.recentPics().subscribe(res=>{
      if (res.data.length==0){
        this.nodata=res.data.length
      }
    })
  }

  showImg(img,orderId,client){
    this.show= true;
    this.image = img;
    this.orderId = orderId;
    this.Client = client;
  }
}
