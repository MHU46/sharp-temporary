import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPicsComponent } from './order-pics.component';

describe('OrderPicsComponent', () => {
  let component: OrderPicsComponent;
  let fixture: ComponentFixture<OrderPicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
