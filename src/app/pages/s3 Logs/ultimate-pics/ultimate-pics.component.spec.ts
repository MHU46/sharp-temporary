import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimatePicsComponent } from './ultimate-pics.component';

describe('UltimatePicsComponent', () => {
  let component: UltimatePicsComponent;
  let fixture: ComponentFixture<UltimatePicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UltimatePicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimatePicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
