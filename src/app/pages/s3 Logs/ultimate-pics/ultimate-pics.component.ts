import { Component, OnInit } from '@angular/core';
import { s3UltimateService } from '../../../models/s3Ultimate.service';
import { MaterialService } from '../../../models/material.service';

@Component({
  selector: 'app-ultimate-pics',
  templateUrl: './ultimate-pics.component.html',
  styleUrls: ['./ultimate-pics.component.css']
})
export class UltimatePicsComponent implements OnInit {

  startDate: any ;
  endDate: any ;
  picsRes: any;
  show: boolean = false;
  image:any;
  id: any;
  Account: any;
  nodata: any;
  
  constructor(  private s3ultimateService : s3UltimateService,
                private materialService : MaterialService ) { }

  ngOnInit() {
    this.getRecentPics();
    this.endDate = this.materialService.getTodayDate();
    this.startDate = this.materialService.getNextDayDate();
  }

  getRecentPics(){
    this.s3ultimateService.recentPics().subscribe(res=>{
      if (res.length || res.data.length != 0){
        this.picsRes = res.data;
      }
      else if(res.data.length == 0){
        this.picsRes=res.data.length
      }
      else if(!res){
        alert("Something went wrong please try again")
      }
    })
  }

  getBetweenPics(){
    this.s3ultimateService.getBetweenPics(this.startDate, this.endDate).subscribe(res=>{
      this.picsRes = res;
      this.getresponse();
    })
  }

  getresponse(){
    this.s3ultimateService.recentPics().subscribe(res=>{
      if (res.data.length==0){
        this.nodata=res.data.length
      }
      else if(!res){
        alert("something went wrong please try again")
      }
    })
  }

  showImg(img,id,account){
    this.show= true;
    this.image = img;
    this.id = id;
    this.Account = account;
  }

}
