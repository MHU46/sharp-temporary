import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AuthService } from '../../models/auth.service';
@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
  password:any;
  email:any;
  constructor(private authenticationService : AuthService,
              private router : Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }
  reset(){
    this.authenticationService.resetPassword(this.email,this.password).subscribe(res =>{
      console.log(res)
      if(res.status == 'success'){
        this.router.navigate(['/login']);
      }
      else if(res.status=='failed'){
        alert('Cant change password,Please try again');
      }
    }) 
  }
}
