import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../models/clients.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  show= false;
  constructor(  private clientService: ClientService  ) { }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers(){
    this.clientService.getAllClients().subscribe(res=>{
      console.log(res);
    })
  }
}
