import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../models/clients.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  show= false;
  product:any;
  bind:any;
  constructor( private clientService: ClientService ) { }

  ngOnInit() {
    this.getAllClients();
    // this.getClientBilling(this.bind);
  }
  test(){
    this.show= true;
  }
  getAllClients(){
    this.clientService.getAllClients().subscribe(res=>{
      this.product=res.data;
      console.log(this.product);
    })
  }
  getClientBilling(bind){
    console.log("inside the funcion")
    this.clientService.getClientBilling(bind).subscribe(res=>{
      // this.product=res.data;
      console.log("Inside client billing")
      console.log(res);
    })
  }
}
