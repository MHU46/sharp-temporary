import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAddPaymentComponent } from './client-add-payment.component';

describe('ClientAddPaymentComponent', () => {
  let component: ClientAddPaymentComponent;
  let fixture: ComponentFixture<ClientAddPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAddPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAddPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
