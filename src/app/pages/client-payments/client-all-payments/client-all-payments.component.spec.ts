import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAllPaymentsComponent } from './client-all-payments.component';

describe('ClientAllPaymentsComponent', () => {
  let component: ClientAllPaymentsComponent;
  let fixture: ComponentFixture<ClientAllPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAllPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAllPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
