import { Component, OnInit } from '@angular/core';
import {  mItemsService } from '../../models/mItem.service'
@Component({
  selector: 'app-medicines',
  templateUrl: './medicines.component.html',
  styleUrls: ['./medicines.component.css']
})
export class MedicinesComponent implements OnInit {
  location: string;
  constructor(  private mItemsService:  mItemsService ) { }

  ngOnInit() {
    this.getAllItems();
    this.getInventory();
  }
  getAllItems(){
    this.mItemsService.allItems().subscribe(res=>{
    console.log(res);
    })
  }

  getInventory(){
    this.mItemsService.allInventory(this.location).subscribe(res=>{
    console.log(res);
    })
  }
}
 