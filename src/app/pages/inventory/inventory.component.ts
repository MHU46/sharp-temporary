import { Component, OnInit } from '@angular/core';
import { LogsService } from '../../models/logs.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  constructor( private logsService: LogsService ) { }

  ngOnInit() {
    this.getAllInventory();
  }
  getAllInventory(){
    this.logsService.getInventory().subscribe(res=>{
      console.log(res)
    })
  }
}
