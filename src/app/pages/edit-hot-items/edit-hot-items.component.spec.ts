import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHotItemsComponent } from './edit-hot-items.component';

describe('EditHotItemsComponent', () => {
  let component: EditHotItemsComponent;
  let fixture: ComponentFixture<EditHotItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditHotItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHotItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
