import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements OnInit {

  constructor(private router : Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
  }
  logout(){
    this.router.navigate(['/login']);
  }
}
