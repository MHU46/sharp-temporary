import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { mainRoutingModule} from './app.routes';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgDatepickerModule } from 'ng2-datepicker';

//Pages
import { AppComponent } from './app.component';
import { SignupComponent } from './pages/signup/signup.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotComponent } from './pages/forgot/forgot.component';
import { ResetComponent } from './pages/reset/reset.component';
import { TopMenuComponent } from './components/top-menu/top-menu.component';
import { LeftMenuComponent } from './components/left-menu/left-menu.component';
import { NavComponent } from './components/nav/nav.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CardComponent } from './components/card/card.component';
import { HotItemsComponent } from './pages/hot-items/hot-items.component';
import { EditHotItemsComponent } from './pages/edit-hot-items/edit-hot-items.component';
import { AllAccountsComponent } from './pages/all-accounts/all-accounts.component';
import { InventoryComponent } from './pages/inventory/inventory.component';
import { TradesComponent } from './pages/trades/trades.component';
import { TransfersComponent } from './pages/transfers/transfers.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { UploadOrdersComponent } from './pages/upload-orders/upload-orders.component';
import { PaymentsComponent } from './pages/payments/payments.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { ItemPricingComponent } from './pages/pricing/item-pricing/item-pricing.component';
import { ClientPricingComponent } from './pages/pricing/client-pricing/client-pricing.component';
import { PriceExceptionComponent } from './pages/pricing/price-exception/price-exception.component';
import { SPriceImportComponent } from './pages/pricing/s-price-import/s-price-import.component';
import { OrderPicsComponent } from './pages/s3 Logs/order-pics/order-pics.component';
import { UltimatePicsComponent } from './pages/s3 Logs/ultimate-pics/ultimate-pics.component';
import { UserLedgerComponent } from './pages/user-ledger/user-ledger.component';
import { MedicinesComponent } from './pages/medicines/medicines.component';
import { ProductGalleryComponent } from './pages/product-gallery/product-gallery.component';
import { ClientInventoryComponent } from './pages/client-inventory/client-inventory.component';
import { ClientTradesComponent } from './pages/client-trades/client-trades.component';
import { ClientTransferComponent } from './pages/client-transfer/client-transfer.component';
import { NewOrderComponent } from './pages/client-orders/new-order/new-order.component';
import { EditOrderComponent } from './pages/client-orders/edit-order/edit-order.component';
import { TransitOrderComponent } from './pages/client-orders/transit-order/transit-order.component';
import { HoldOrderComponent } from './pages/client-orders/hold-order/hold-order.component';
import { AllOrdersComponent } from './pages/client-orders/all-orders/all-orders.component';
import { NewClientInvoiceComponent } from './pages/client-invoices/new-client-invoice/new-client-invoice.component';
import { AllClientInvoiceComponent } from './pages/client-invoices/all-client-invoice/all-client-invoice.component';
import { ClientBillingComponent } from './pages/client-payments/client-billing/client-billing.component';
import { ClientLedgerComponent } from './pages/client-payments/client-ledger/client-ledger.component';
import { ClientAddPaymentComponent } from './pages/client-payments/client-add-payment/client-add-payment.component';
import { ClientAllPaymentsComponent } from './pages/client-payments/client-all-payments/client-all-payments.component';
import { StaffInventoryComponent } from './pages/staff/staff-inventory/staff-inventory.component';
import { StaffOrdersComponent } from './pages/staff/staff-orders/staff-orders.component';
import { StaffTrackComponent } from './pages/staff/staff-track/staff-track.component';
import { StaffUploadPicComponent } from './pages/staff/staff-upload-pic/staff-upload-pic.component';
import { StaffVerifyOrderComponent } from './pages/staff/staff-verify-order/staff-verify-order.component';
import { StaffMedicinesComponent } from './pages/staff/staff-medicines/staff-medicines.component';
import { StaffTradesComponent } from './pages/staff/staff-trades/staff-trades.component';
import { StaffTransitsComponent } from './pages/staff/staff-transits/staff-transits.component';
import { StaffTransfersComponent } from './pages/staff/staff-transfers/staff-transfers.component';
import { StaffBalancesComponent } from './pages/staff/staff-balances/staff-balances.component';
import { StaffUnusedTrackComponent } from './pages/staff/staff-unused-track/staff-unused-track.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';

//Services
import { AuthService } from './models/auth.service';
import { ClientService } from './models/clients.service';
import { LogsService } from './models/logs.service';
import { OrdersService } from './models/orders.service';
import { AdminService } from './models/admin.service';
import { PaymentsService } from './models/payments.service';
import { InvoicesService } from './models/invoices.service';
import { PriceExceptionService } from './models/priceException.service';
import { PriceImportService } from './models/sPriceImport.service';
import { mItemsService } from './models/mItem.service';
import { s3OrderLogsService } from './models/s3OrderLog.service';
import { s3UltimateService } from './models/s3Ultimate.service';
import { uTransfersService } from './models/uTransfer.service';
import { MaterialService } from './models/material.service';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    ForgotComponent,
    ResetComponent,
    TopMenuComponent,
    LeftMenuComponent,
    NavComponent,
    DashboardComponent,
    CardComponent,
    HotItemsComponent,
    EditHotItemsComponent,
    AllAccountsComponent,
    InventoryComponent,
    TradesComponent,
    TransfersComponent,
    OrdersComponent,
    UploadOrdersComponent,
    PaymentsComponent,
    InvoicesComponent,
    ItemPricingComponent,
    ClientPricingComponent,
    PriceExceptionComponent,
    SPriceImportComponent,
    OrderPicsComponent,
    UltimatePicsComponent,
    UserLedgerComponent,
    MedicinesComponent,
    ProductGalleryComponent,
    ClientInventoryComponent,
    ClientTradesComponent,
    ClientTransferComponent,
    NewOrderComponent,
    EditOrderComponent,
    TransitOrderComponent,
    HoldOrderComponent,
    AllOrdersComponent,
    NewClientInvoiceComponent,
    AllClientInvoiceComponent,
    ClientBillingComponent,
    ClientLedgerComponent,
    ClientAddPaymentComponent,
    ClientAllPaymentsComponent,
    StaffInventoryComponent,
    StaffOrdersComponent,
    StaffTrackComponent,
    StaffUploadPicComponent,
    StaffVerifyOrderComponent,
    StaffMedicinesComponent,
    StaffTradesComponent,
    StaffTransitsComponent,
    StaffTransfersComponent,
    StaffBalancesComponent,
    StaffUnusedTrackComponent,
    UserProfileComponent
  ],
  imports: [
    BrowserModule,
    mainRoutingModule,
    HttpModule,
    FormsModule,
    NgDatepickerModule

  ],
  providers: [  AuthService,
                ClientService,
                LogsService,
                OrdersService,
                AdminService,
                PaymentsService,
                InvoicesService,
                PriceExceptionService,
                PriceImportService,
                mItemsService,
                s3OrderLogsService,
                s3UltimateService,
                uTransfersService,
                MaterialService
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }
