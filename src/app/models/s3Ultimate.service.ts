import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { 
        Jsonp,
        Http,
        Response,
        Headers,
        RequestOptions
} from '@angular/http';
import { BASE_URL } from './constants';
import 'rxjs/add/operator/map';
import { MaterialService } from './material.service';

@Injectable()

export class s3UltimateService {

    baseurl = BASE_URL;
        
    constructor(    private http : Http ,
                    private materialService : MaterialService) { }

_getRequestOptions(){
    let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded' }); 
    return new RequestOptions({ headers: headers });
}
    
    recentPics() { 
        let url = this.baseurl + 'v2/uRecentPics';
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error || 'Server error getting brands'))
    }

    getBetweenPics( startDate,endDate ) { 
        let eDate = this.materialService.ConvertEndDate(endDate);
        let sDate = this.materialService.ConvertStartDate(startDate)
        let url = this.baseurl + 'v2/ubetween?'+'endDate='+eDate+'&'+'startDate='+sDate;
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error || 'Server error getting brands'))
    }

}