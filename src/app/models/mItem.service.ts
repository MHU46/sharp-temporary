import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { 
        Jsonp,
        Http,
        Response,
        Headers,
        RequestOptions
} from '@angular/http';
import { BASE_URL } from './constants';
import 'rxjs/add/operator/map';

@Injectable()

export class mItemsService {

    baseurl = BASE_URL;
        
    constructor( private http : Http ) { }

_getRequestOptions(){
    let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded' }); 
    return new RequestOptions({ headers: headers });
}
    
    itemPricing() { 
        let url = this.baseurl + 'v2/itemPricing';
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error || 'Server error getting brands'))
    }

    allItems() { 
        let url = this.baseurl + 'v2/allItems';
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error || 'Server error getting brands'))
    }

    allInventory(location) { 
        let url = this.baseurl + 'v2/inventory/'+location;
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error || 'Server error getting brands'))
    }

   

}