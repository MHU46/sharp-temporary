import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Injectable()

export class MaterialService {
         
    today: any;
    nextDay: any;
    startDate: any ;
    endDate: any ;
    Day:any;
    DayString:any;
    Month:any;
    MonthString:any;
    Year:any;

    constructor( ) { }

    getTodayDate(){
        this.today = new Date();
        return this.today;
    }

    getNextDayDate(){
        this.today = new Date();
        this.nextDay = new Date(this.today.setDate(new Date().getDate()+1));
        return this.nextDay;
    }

    ConvertStartDate(startDate){
        console.log(startDate)
        this.today = startDate;
        this.Day = this.today.getDate();
        this.DayString =this.Day.toString()
        this.Month = this.today.getMonth()+1;
        this.MonthString =this.Month.toString()
        this.Year = this.today.getFullYear();
        if (this.DayString.length==1){
        this.Day ="0"+this.Day
        }
        if (this.MonthString.length==1){
        this.Month="0"+this.Month
        }
        this.startDate = this.Year+'-'+this.Month+'-'+this.Day;
        return this.startDate 
    }

    ConvertEndDate(endDate){
        console.log(endDate)
        this.today = endDate;
        this.Day = this.today.getDate();
        this.DayString =this.Day.toString()
        this.Month = this.today.getMonth()+1;
        this.MonthString =this.Month.toString()
        this.Year = this.today.getFullYear();
        if (this.DayString.length==1){
        this.Day ="0"+this.Day
        }
        if (this.MonthString.length==1){
        this.Month="0"+this.Month
        }
        this.endDate = this.Year+'-'+this.Month+'-'+this.Day;
        return this.endDate 
    }


}