import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { 
        Jsonp,
        Http,
        Response,
        Headers,
        RequestOptions
} from '@angular/http';
import { BASE_URL } from './constants';
import 'rxjs/add/operator/map';

@Injectable()

export class AuthService {

    private isLoggedIn: boolean;
    private userId : string;
    private token: string;    
    baseurl = BASE_URL;
        
    constructor( private http : Http ) { }

_setLogin(user,token){

    this.isLoggedIn = true;
    // this.userId = user._id;
    // this.token = token; 
}

_getRequestOptions(){
    let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded' }); 
    return new RequestOptions({ headers: headers });
}
    
    signUp(username:any,email:any,password:any,role:any){
        
        let dataString ="username="+username+"&"+"email="+email+"&"+"password="+password+"&"+"role="+role;
        // let body = {
        //     "username": username,
        //     "password": password,
        //     "email": email,
        //     "role": role
        // };
        let url = this.baseurl + 'v2/Signup';
        let options = this._getRequestOptions();
        return this.http.post(url, dataString, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }

    login(username:any,password:any) { 
            
        let dataString = "username="+username+"&"+"password="+password;
        // let body = {
        //     "username": username,
        //     "password": password
        // };
        let url = this.baseurl + 'v2/Login';
        let options = this._getRequestOptions();
        return this.http.post(url, dataString, options)
            .map((res:Response) => { 
                        let udata = res.json();
                        if(!res){
                                return false;
                        }
                        if(udata.status == "success"){
                                this._setLogin(udata.user,udata.token,);
                    // return true;
                        }
            return udata;
            })
            .catch((error:any) => Observable.throw(error || 'Server error getting brands'))
    }

    forgotPassword(email){
       
        let dataString = "email="+email;
        // let body = {
        //     "email": email,
        // };
        let url = this.baseurl + 'v2/forgot';
        let options = this._getRequestOptions();
        return this.http.post(url, dataString, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }
     
    resetPassword(password:any,email:any){

        let dataString = "password="+password+"&"+"email="+email;
        // let body = {
        //     "password": password,
        //     "email": email
        // };
        let url = this.baseurl + 'v2/reset';
        let options = this._getRequestOptions();
        return this.http.post(url, dataString, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }

}