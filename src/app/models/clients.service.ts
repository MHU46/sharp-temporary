import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { 
        Jsonp,
        Http,
        Response,
        Headers,
        RequestOptions
} from '@angular/http';
import { BASE_URL } from './constants';
import 'rxjs/add/operator/map';

@Injectable()

export class ClientService {

    baseurl = BASE_URL;
        
    constructor( private http : Http ) { }

_getRequestOptions(){
    let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded' }); 
    return new RequestOptions({ headers: headers });
}
    
    updateClient(username:any,email:any,password:any){
        
        let dataString ="username="+username+"&"+"email="+email+"&"+"password="+password;
        let url = this.baseurl + 'v2/';
        let options = this._getRequestOptions();
        return this.http.post(url, dataString, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }

    getAllClients() { 

        let url = this.baseurl + 'v2/allClients';
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error || 'Server error getting brands'))
    }

    getActiveClients(){
       
        let url = this.baseurl + 'v2/activeClients';
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }
     
    getClientPricing(){

        let url = this.baseurl + 'v2/clientPricing';
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }

    getAllClientAccounts(){

        let url = this.baseurl + 'v2/allAccounts';
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }
    getClientBilling(bind){
        let url = this.baseurl + 'v2/clientBilling/'+bind;
        let options = this._getRequestOptions();
        return this.http.get(url, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json() || 'Server error getting brands'))
    }

}